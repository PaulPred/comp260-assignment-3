﻿using UnityEngine;
using System.Collections;

public class BinMove : MonoBehaviour {


	private Animator animator;
	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator>();
		animator.SetBool ("Start", false);
	}
	
	// Update is called once per frame
	void Update () {
		if(Score.score > 2){
	        animator.SetBool ("Start", true);
		}
	}
}
