﻿using UnityEngine;
using System.Collections;

public class EnemySpawn : MonoBehaviour {

	public float StartTime;
	public GameObject EnemyPrefab;
	private float InstantiationTimer = 2.0f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		CreatePrefab ();
	}

	void CreatePrefab(){
		InstantiationTimer -= Time.deltaTime;
		if (InstantiationTimer <= 0) {
			GameObject enemy = Instantiate (EnemyPrefab);
			enemy.transform.position = transform.position;
			InstantiationTimer = Random.Range(1,11);
		}
	}
}
