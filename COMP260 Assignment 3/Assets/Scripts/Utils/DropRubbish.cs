﻿using UnityEngine;
using System.Collections;

public class DropRubbish : MonoBehaviour {

	// Use this for initialization
	void Start () {

	}
	
	public GameObject RubbishPrefab;

	void Update () {
		// when the button is pushed, drop a piece of rubbish
		if (Input.GetButtonDown("Fire1")) {

			GameObject Rubbish = Instantiate(RubbishPrefab);
			// the rubbish starts at fist
			Rubbish.transform.position = transform.position;

		}
	}
	void OnCollisionEnter2D(Collision2D collision) {
		// Destroy the rubbish
		if (collision.gameObject.name == "Mother") {
			Destroy (collision.gameObject);

			Debug.Log ("collision");
		}
	}
}
